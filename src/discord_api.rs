use std::env::{self, VarError};
use serenity::prelude::*;
use dotenv::dotenv;

use lazy_static::*;

static DISCORD_TOKEN_VAR_NAME: &str = "DISCORD_TOKEN";
static DISCORD_TOKEN_FAIL_MESSAGE: &str = "Could not get discord token... do you have an `.env` file in the root dir???? (dumbass idiot!!!)";

lazy_static! {
    pub static ref DISCORD_GATEWAY_INTENTS: GatewayIntents =    GatewayIntents::GUILD_MESSAGES |
                                                            GatewayIntents::DIRECT_MESSAGES |
                                                            GatewayIntents::MESSAGE_CONTENT;
}

#[derive(Clone)]
pub struct DiscordAPIInfo {
    pub token: String,
    pub intents: GatewayIntents
}

impl DiscordAPIInfo {
    /// Fetches discord API token
    fn get_discord_token() -> Result<String, VarError> {
        env::var(DISCORD_TOKEN_VAR_NAME)
    }

    /// Fetches discord API info
    pub fn init() -> Option<DiscordAPIInfo>  {
        dotenv().expect(DISCORD_TOKEN_FAIL_MESSAGE);

        match DiscordAPIInfo::get_discord_token() {
            Ok(t) => Some(DiscordAPIInfo {
                token: t,
                intents: *DISCORD_GATEWAY_INTENTS
            }),
            Err(_) => return None
        }
    }
}