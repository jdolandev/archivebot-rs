use serenity::prelude::*;
use regex::Regex;
use std::collections::HashSet;

use lazy_static::*;

use super::discord_api::*;
use super::response_handler::*;

/// The error message displayed 
static ARCHIVEBOT_INFO_FAILURE_MESSAGE: &str = "Error fetching discord API info";
static ARCHIVEBOT_CLIENT_FAILURE_MESSAGE: &str = "Error connecting to client. Yeah, I dunno either, big guy. Maybe plug ur ethernet cable in???";

lazy_static! {
    /// The regex used to verify whether or not an input string is a valid URL.
    pub static ref URL_REGEX:Regex = Regex::new(r"(https?://[^\s/$.?#].[^\s]*)").unwrap();
    
    /// The Regex used to extract a domain. Most likely redundant because I'm fucking dumb as hell
    pub static ref DOMAIN_REGEX:Regex = Regex::new(r"^(?:https?://)?(?:www\.)?([^/?#]+)").unwrap();
    
    ///
    pub static ref URL_IGNORE_COMPONENTS: HashSet<&'static str> = [ "www",
                                                                    "http", 
                                                                    "https"].iter().cloned().collect();
                                                                
    // TODO(Joe): Move this to a config file or something.
    /// Collection of the sites with hard paywalls (according to https://www.reddit.com/r/worldnews/wiki/paywalls/)
    pub static ref RAT_BASTARDS_LIST: HashSet<&'static str> = [
        "ft.com",
        "latimes.com",
        "telegraph.co.uk",
        "wired.com",
        "thetimes.co.uk",
        "thesundaytimes.co.uk",
        "wsj.com",
        "kyivpost.com",
        "scmp.com",
        "nationalpost.com",
        "haaretz.com",
        "bostonglobe.com",
        "theaustralian.com.au",
        "smh.com.au",
        "theage.com.au",
        "washingtonpost.com",
        "nytimes.com",
        "bloomberg.com",

    ].iter().cloned().collect();
}


/// The rat bastard who runs the show
#[allow(unused)]
pub struct ArchiveBot {
    /// Discord API info - Functionally useless outside of debugging
    api_info: DiscordAPIInfo,

    /// The serenity client which does the actual work
    client: serenity::Client
}

impl ArchiveBot {
    /// Returns a URL's base domain. This assumes a valid URL string
    pub fn get_url_domain(url: &str) -> Option<String> {
        DOMAIN_REGEX.captures(url).and_then(|cap| {
            cap.get(1).map(|domain| {
                domain.as_str()
                        .split('.')
                        .skip_while(|&part| URL_IGNORE_COMPONENTS.contains(part))
                        .collect::<Vec<&str>>()
                        .join(".")
            })
        })
    }

    /// Returns a vector of URLs from a given string if any exist in it. Otherwise None
    pub fn extract_urls(text: &str) -> Option<Vec<String>> {
        let urls: Vec<String> = URL_REGEX
            .captures_iter(text)
            .filter_map(|u| {
                        u.get(0)
                        .map(|m| {
                            m.as_str().to_string()
                        })
                    }
                ).collect();

        // TODO(Joe): It's dumb as fuck to waste cycles casting this to a slice only to return the vector anyway. Maybe uhhh... don't be a dumbass???
        match urls.as_slice() {
            [] => None,
            _ => Some(urls)
        }
    }

    /// Creates a new archiveBot
    pub async fn new() -> ArchiveBot {
        let api_info = DiscordAPIInfo::init().expect(ARCHIVEBOT_INFO_FAILURE_MESSAGE);
        let client = Client::builder(&api_info.token, api_info.intents)
                                            .event_handler(ResponseHandler)
                                            .await
                                            .expect(ARCHIVEBOT_CLIENT_FAILURE_MESSAGE);

        ArchiveBot {
            api_info,
            client
        }
    }

    /// The function which runs the bot
    pub async fn run(&mut self) {
        if let Err(why) = self.client.start().await {
            let error_message = format!("Client error: {why:?}");
            println!("{}", error_message);
        }
    }
}