use serenity::async_trait;
use serenity::model::channel::Message;
use serenity::prelude::*;
use rand::seq::SliceRandom;
use lazy_static::*;

use crate::archive_bot::RAT_BASTARDS_LIST;
use crate::ArchiveBot;

pub struct ResponseHandler;
lazy_static! {
    static ref CUTE_LIL_MESSAGES: Vec<&'static str> = vec![
        "WARNING... WARNING... RAT BASTARD DETECTED... BYPASSING PAYWALLS... ",
        "BEEP BOOP UNFUCKING YOUR LINK... ",
        "SMELLS LIKE RAT BASTARD IN HERE...",
        "UNPAYING YOUR WALLS...",
        "PAYWALL? WHAT PAYWALL???? ",
        "FREE CONTENT HERE I COME!",
        "SCALING THE SUBSCRIPTION WALL...",
        "VIP ACCESS UNLOCKED...",
        "JUMPING OVER THE SUBSCRIPTION HURDLE...",
        "SILENCING THE GATEKEEPERS...",
        "I'M A PAYWALL ESCAPE ARTIST",
        "NO SUBSCRIPTION NEEDED...",
        "UNLOCKING THE VIP LOUNGE...",
        "[PIRATE MODE ACTIVATED]"
    ];
}

impl ResponseHandler {
    /// Writes a response to user input
    async fn write_response(&self, msg: &Message, ctx: &Context, res: &str) {
        match msg.channel_id.say(&ctx.http, res).await {
            Err(e) => {
                println!("Error sending message: {e:?}");
            },
            _ => ()
        }
    }
}

#[async_trait]
impl EventHandler for ResponseHandler {
    async fn message(&self, ctx: Context, msg: Message) {
        if !msg.is_own(&ctx.cache) {
            // Trigger only when a user has entered a valid url.
            match ArchiveBot::extract_urls(&msg.content) {
                Some(urls) => {
                    // Get a list of the domains posted
                    let blacklisted_domains: Vec<String> = urls.into_iter()
                                                    .filter_map(|url| {
                                                        let domain = ArchiveBot::get_url_domain(&url)?;
                                                        if RAT_BASTARDS_LIST.contains(domain.as_str()) {
                                                            Some(format!("https://12ft.io/{}", url))
                                                        } else {
                                                            None
                                                        }
                                                    }).collect();
                    if !blacklisted_domains.is_empty() {
                        let res = blacklisted_domains.join("\n");
                        let preamble = CUTE_LIL_MESSAGES.choose(&mut rand::thread_rng()).unwrap();
                        let fmt_res = format!("{}: {}", preamble, res);
                        self.write_response(&msg, &ctx, &fmt_res).await
                    }
                },
                None => (),
            }
        }
    }
}
