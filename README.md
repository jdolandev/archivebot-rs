<a name="readme-top"></a>

<br />
<div align="center">
  <h3 align="center">ARCHIVEBOT (FREEDOMBOT)</h3>

  <p align="center">
    <b>A really shitty Discord bot that automatically provides uncensored links to news articles</b>
</div>

<!-- ABOUT THE PROJECT -->
## About The Project

Ideally, this will provide archive.is (and all its variants) links to sites when a user posts a link to a paywalled article. 
As of right now it just generates 12ft links because there's no reliable way to fetch an archive, but it's still plenty useful anyway.

To be completely honest it's probably buggy as fuck, I wrote this in an hour so don't expect too much here.

<!-- GETTING STARTED -->
## Getting Started


1. Get a [discord bot API key](https://discord.com/developers/applications/)
2. Clone the repo
```sh
git clone https://gitlab.com/jdolandev/archivebot-rs.git
```
3. Enter your Discord API token in `.env` located in the root folder, it should look like
```
DISCORD_TOKEN=YOUR_TOKEN
```
4. Run it
```
cargo run --release
```
5. Don't commit `.env` to your repo, stupid!

<!-- ROADMAP -->
## Roadmap

- [x] 12ft link support
- [x] Archive.is support
- [ ] medium.com support
- [ ] Minified url support
- [ ] Archival Commands
- [ ] Screenshot generation
- [ ] Self hosted archives

See the [open issues](https://gitlab.com/jdolandev/archivebot-rs/-/issues) for a full list of proposed features (and known issues).

<!-- LICENSE -->
## License

Distributed under the APACHE 2.0 License. See `LICENSE.txt` for more information.

<!-- CONTACT -->
## Contact

Joe Dolan - jdolandev@gmail.com

<!-- Project Link: [https://gitlab.com/jdolandev/archivebot-rs](https://gitlab.com/jdolandev/archivebot-rs) -->

<p align="right">(<a href="#readme-top">back to top</a>)</p>